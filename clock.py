#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import, unicode_literals

import sys

from RPLCD_i2c import CharLCD
from RPLCD_i2c import Alignment, CursorMode, ShiftMode
from RPLCD_i2c import cursor, cleared
from time import strftime, sleep
from datetime import datetime

try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus
from bme280 import BME280

# Initialise the BME280
bus = SMBus(1)
bme280 = BME280(i2c_dev=bus)

# build big digits
def disp_number(lcd_char, position):

    if lcd_char=="0" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(4))
        lcd.write_string(unichr(0))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(7))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="1" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(0))
        lcd.write_string(unichr(5))
        lcd.write_string(unichr(254))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(1))

    elif lcd_char=="2" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(1))

    elif lcd_char=="3" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(0))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="4" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(255))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(6))

    elif lcd_char=="5" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(2))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="6" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(4))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(2))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(7))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="7" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(0))
        lcd.write_string(unichr(0))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(6))

    elif lcd_char=="8" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(4))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(7))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="9" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(2))
        lcd.write_string(unichr(5))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(1))
        lcd.write_string(unichr(6))

    elif lcd_char=="C" :
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(255))
        lcd.write_string(unichr(2))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))

    else:
        lcd.cursor_pos = (0, position)
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))
        lcd.cursor_pos = (1, position)
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))
        lcd.write_string(unichr(254))

    return lcd_char;

def clock_dots():
    # display two dots
    lcd.cursor_pos = (0, 6)
    lcd.write_string(unichr(3))
    lcd.cursor_pos = (1, 6,)
    lcd.write_string(unichr(3))
    lcd.cursor_pos = (0, 13)
    lcd.write_string(unichr(3))
    lcd.cursor_pos = (1, 13,)
    lcd.write_string(unichr(3))

    sleep(0.5)

    # remove the two dots
    lcd.cursor_pos = (0, 6)
    lcd.write_string(unichr(254))
    lcd.cursor_pos = (1, 6)
    lcd.write_string(unichr(254))
    lcd.cursor_pos = (0, 13)
    lcd.write_string(unichr(254))
    lcd.cursor_pos = (1, 13)
    lcd.write_string(unichr(254))

    sleep(0.5)

def clock_hour(digits):
    lcd.cursor_pos = (0, 0)
    lcd.write_string('                ')
    lcd.cursor_pos = (1, 0)
    lcd.write_string('                ')
    disp_number(digits[0], 0)
    disp_number(digits[1], 3)
    disp_number(digits[2], 7)
    disp_number(digits[3], 10)
    disp_number(digits[4], 14)
    disp_number(digits[5], 17)
    date = datetime.now().strftime('%d/%m/%Y %a')
    lcd.cursor_pos = (2, 0)
    lcd.write_string(date)
    temperature = bme280.get_temperature()
    pressure = bme280.get_pressure()
    pressure = round(pressure * 0.750061683, 2)
    humidity = bme280.get_humidity()
    lcd.cursor_pos = (3, 0)
    lcd.write_string('{:04.1f}C {:04.1f}Hg {:04.1f}%'.format(temperature, pressure, humidity))

def main():

    try:
        input = raw_input
    except NameError:
        pass

    try:
        unichr = unichr
    except NameError:
        unichr = chr

    old_time = 0
    counter = 0

    # custom symbols
    lcd.clear()
    top_line = (31,31,0,0,0,0,0,0)
    bottom_line = (0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111, 0b11111)
    both_lines = (0b11111, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111, 0b11111)
    dot = (0b00000, 0b00000, 0b00000, 0b11000, 0b11000, 0b00000, 0b00000, 0b00000)
    up_left = (0b00111, 0b01111, 0b01111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111)
    up_right = (0b11100, 0b11110, 0b11110, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111)
    bot_right = (0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b11110, 0b11110, 0b11100)
    bot_left = (0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0b01111, 0b01111, 0b00111)

    lcd.create_char(0, top_line)
    lcd.create_char(1, bottom_line)
    lcd.create_char(2, both_lines)
    lcd.create_char(3, dot)
    lcd.create_char(4, up_left)
    lcd.create_char(5, up_right)
    lcd.create_char(6, bot_right)
    lcd.create_char(7, bot_left)

    #main loop
    while True:
        counter += 1
        new_time = datetime.now().strftime('%H%M%S')
        date = datetime.now().strftime('%d/%m/%Y %a')
        temperature = bme280.get_temperature()
        pressure = bme280.get_pressure()
        pressure = round(pressure * 0.750061683, 2)
        humidity = bme280.get_humidity()

        # time changed, update LCD buffer
        if new_time!=old_time :
            digits = str(new_time)
            tens_hour = disp_number(digits[0], 0)
            hour = disp_number(digits[1], 3)
            tens_minutes = disp_number(digits[2], 7)
            minutes = disp_number(digits[3], 10)
            tens_seconds = disp_number(digits[4], 14)
            seconds = disp_number(digits[5], 17)
            lcd.cursor_pos = (2, 0)
            lcd.write_string(date)
            lcd.cursor_pos = (3, 0)
            lcd.write_string('{:04.1f}C {:04.1f}Hg {:04.1f}%'.format(temperature, pressure, humidity))
            old_time = new_time
            cur_hour = int(tens_hour)*10 + int(hour)

            # enable backlight by night
            # lcd.set_backlight(cur_hour>=01 or cur_hour<9)

        # dots blink
        clock_dots()

#        if counter==15 :
#            counter=0
            # date display
#            clock_date(digits, month, day_name)
            # temperature display
#            clock_temp()
            # temperature display
#            clock_hour(digits)

if __name__ == '__main__':

  lcd = CharLCD(address=0x27, port=1, cols=20, rows=4, dotsize=8)

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    lcd.clear()
    lcd.set_backlight(False)
    lcd.home()
